//Create - 5 members of fictional group/musical band to users collection
db.users.insertMany([
{
    "firstName": "Dan",
    "lastName": "Reynolds",
    "email": "danreynolds2@gmail.com",
    "password": "te52wq",
    "isAdmin": false
},
{
    "firstName": "Daniel Wayne",
    "lastName": "Sermon",
    "email": "Wayneser_90@gmail.com",
    "password": "22uiyr",
    "isAdmin": false
},
{
    "firstName": "Daniel",
    "lastName": "Platzman",
    "email": "platzdan214@gmail.com",
    "password": "polh53",
    "isAdmin": false
},
{
    "firstName": "Ben",
    "lastName": "McKee",
    "email": "mcKeecool42@gmail.com",
    "password": "jjhd591",
    "isAdmin": false
},
{
    "firstName": "Andrew",
    "lastName": "Tolman",
    "email": "tolmanandrew78@gmail.com",
    "password": "6312ws",
    "isAdmin": false
}

])

//add 3 new courses to courses collection
db.courses.insertMany([
{
    "name": "IT 101",
    "price": 2600,
    "isActive": false
},
{
    "name": "DBMS 101",
    "price": 2300,
    "isActive": false
},
{
     "name": "Data Structures 101",
    "price": 3100,
    "isActive": false
}


])

//find all regular/non-admin users
db.users.find({"isAdmin": false})

//Update the first user in the users collection as an admin
db.users.updateOne({"isAdmin": false}, {$set: {"isAdmin": true}})

//Update one of the courses as active
db.courses.updateOne({"isActive": false}, {$set: {"isActive": true}})

//Delete all inactive courses
db.courses.deleteMany({"isActive": false})




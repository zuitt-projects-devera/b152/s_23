//Create

//db.collection.insertOne({}) - allow us to insert/create a new document.

//db.collection.insertMany([()]) - allow us to insert/create multiple documents.

//Read
//db.collection.find() - allow us to find/return all documents in a collection
//db.collection.findOne({}) - allow us to find/return the first document in a collection.
//db.collection.find({[}"criteria":"value"}) - allow us to find/return ALL documents that matches 
//the criteria in a collection.
//db.collection.findOne({"criteria":"value"}) - allow us to find/return the FIRST document that
//matches the criteria in a collection

//Update
//db.collection.updateOne({"criteria": "value"}, {$set: ("fieldToBeUpdated": "Updated Value"))})
//allows us to update the first item that matches our criteria

//db.collection.updateOne({}, {$set: ("fieldToBeUpdated": "Updated Value"))})
//allows us to update the first item in the collection

//db.collection.updateMany({"criteria": "value"}, {$set: ("fieldToBeUpdated": "Updated Value"))})
//allows us to update all items in the collection

//db.collection.updateMany({}, {$set: ("fieldToBeUpdated": "Updated Value"))})
//allows us to update all of the documents (add new fields)


db.users.updateOne({"superpower", "Superhuman strength"}, {$set: {"superpower": "Superhuman Strength"}})
//add new criteria
db.users.updateOne({"username": "spiDerMan_69"}, {$set: {"isAdmin":false}}) // automatically adds
//criteria if the value doesn't exist in a document


//Delete
//db.collection.deleteOne({"criteria": "value"})
//db.collection.deleteMany({"criteria": "value"})
//db.collection.deleteMany({})
//db.user.delete("{superpower": "Superhuman Strength"})